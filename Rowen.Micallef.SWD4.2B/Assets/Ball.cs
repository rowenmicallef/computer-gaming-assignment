﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour {

    int sideStart;
    Rigidbody2D ballBody;

    public void RandomSpot()
    {
        Scene currentLevel = SceneManager.GetActiveScene();

        int xDirection = Random.Range(0, 2); // Random Genrator for x Axis
        int yDirection = Random.Range(0, 2); // Random Genrator for y Axis
        Vector3 ballDirection = new Vector3();

        if (xDirection == 0)
        {
            ballDirection.x = -9f;
        }

        if (xDirection == 1)
        {
            ballDirection.x = 9f;
        }

        if (yDirection == 0)
        {
            ballDirection.y = -9f;
        }

        if (yDirection == 1)
        {
            ballDirection.y = 9f;
        }
        ballBody = GetComponent<Rigidbody2D>();
        ballBody.velocity = ballDirection;

        if (currentLevel == SceneManager.GetSceneByName("Level1"))
        {
            if (xDirection == 0)
            {
                ballDirection.x = -7f;
            }

            if (xDirection == 1)
            {
                ballDirection.x = 7f;
            }

            if (yDirection == 0)
            {
                ballDirection.y = -7f;
            }

            if (yDirection == 1)
            {
                ballDirection.y = 7f;
            }

            ballBody = GetComponent<Rigidbody2D>();
            ballBody.velocity = ballDirection;

        }

        if (currentLevel == SceneManager.GetSceneByName("Level2"))
        {
            if (xDirection == 0)
            {
                ballDirection.x = -9f;
            }

            if (xDirection == 1)
            {
                ballDirection.x = 9f;
            }

            if (yDirection == 0)
            {
                ballDirection.y = -9f;
            }

            if (yDirection == 1)
            {
                ballDirection.y = 9f;
            }
            ballBody = GetComponent<Rigidbody2D>();
            ballBody.velocity = ballDirection;

        }

        if (currentLevel == SceneManager.GetSceneByName("Level3"))
        {
            if (xDirection == 0)
            {
                ballDirection.x = -10f;
            }

            if (xDirection == 1)
            {
                ballDirection.x = 10f;
            }

            if (yDirection == 0)
            {
                ballDirection.y = -10f;
            }

            if (yDirection == 1)
            {
                ballDirection.y = 10f;
            }

            ballBody = GetComponent<Rigidbody2D>();
            ballBody.velocity = ballDirection;

        }

    }


    // Use this for initialization
    void Start () {
        RandomSpot();
        
    }

    //Update is called once per frame
    void Update () {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "rPaddle")
        {
            System.Threading.Thread.Sleep(500);
            RandomSpot();
            transform.position = new Vector3(0, 0, 0);

        }

        if (collision.gameObject.tag == "lPaddle")
        {

            System.Threading.Thread.Sleep(500);
            RandomSpot();
            transform.position = new Vector3(0, 0, 0);
            
        }

    }
}
