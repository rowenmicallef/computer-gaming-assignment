﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameController : MonoBehaviour {

    public void startGame() //Start the game (go to level 1)
    {
        SceneManager.LoadScene("Level1");
    }

    public void exitGame()
    {
        Application.Quit(); // Does not work in edit, needs an actual exacutable
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
