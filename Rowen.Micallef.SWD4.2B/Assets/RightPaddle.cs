﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightPaddle : MonoBehaviour {

    // Use this for initialization
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 mouseX = Input.mousePosition;

        Vector3 paddleX = Camera.main.ScreenToWorldPoint(mouseX);

        //Position y of mouse
        transform.position = new Vector3(10f, Mathf.Clamp(paddleX.y , -4f, 4f));

    }
}
