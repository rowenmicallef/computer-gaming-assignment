﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;   


public class rightGoalScript : MonoBehaviour {
    int rightScore;
	// Use this for initialization
	void Start () {
        rightScore = 0;
        GameObject.Find("RightScore").GetComponent<Text>().text = "Left: " + rightScore.ToString();
    }
	
	// Update is called once per frame
	void Update () {

        //Change the level after a certain score is reached
        Scene currentLevel = SceneManager.GetActiveScene();

        //Level 1 to level 2
        if (currentLevel == SceneManager.GetSceneByName("Level1"))
        {
            if (rightScore == 3)
            {
                rightScore = 0;
                SceneManager.LoadScene("Level2");
            }
        }

        //Level 2 to level 3
        if (currentLevel == SceneManager.GetSceneByName("Level2"))
        {
            if (rightScore == 6)
            {
                rightScore = 0;
                SceneManager.LoadScene("Level3");
            }
        }

        //Level 3 to end game
        if (currentLevel == SceneManager.GetSceneByName("Level1"))
        {
            if (rightScore == 9)
            {
                rightScore = 0;
                SceneManager.LoadScene("EndGame");
            }
        }
        
    }

        private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "bScore")
        {
            rightScore++;
            GameObject.Find("RightScore").GetComponent<Text>().text = "Left: "+rightScore.ToString();

        }

    }
}
