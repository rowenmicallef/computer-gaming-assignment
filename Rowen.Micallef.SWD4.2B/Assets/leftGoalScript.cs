﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class leftGoalScript : MonoBehaviour {

    int leftScore;
    
    // Use this for initialization
    void Start () {
        leftScore = 0 ;
        GameObject.Find("LeftScore").GetComponent<Text>().text = "Right: " + leftScore.ToString();
    }
	
	// Update is called once per frame
	void Update () {
        //Change the level after a certain score is reached
        Scene currentLevel = SceneManager.GetActiveScene();
        //Level 1 to level 2
        if (currentLevel == SceneManager.GetSceneByName("Level1"))
        {
            if (leftScore == 3)
            {
                leftScore = 0;
                SceneManager.LoadScene("Level2");
            }
        }

        //Level 2 to level 3
        if (currentLevel == SceneManager.GetSceneByName("Level2"))
        {
            if (leftScore == 6)
            {
                leftScore = 0;
                SceneManager.LoadScene("Level3");
            }
        }

        //Level 3 to end game
        if (currentLevel == SceneManager.GetSceneByName("Level1"))
        {
            if (leftScore == 9)
            {
                leftScore = 0;
                SceneManager.LoadScene("Win");
            }
        }
        GameObject.Find("LeftScore").GetComponent<Text>().text = "Right: " + leftScore.ToString();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "bScore")
        {
            leftScore++;
            GameObject.Find("LeftScore").GetComponent<Text>().text = "Lef: " + leftScore.ToString();

        }


    }
}
