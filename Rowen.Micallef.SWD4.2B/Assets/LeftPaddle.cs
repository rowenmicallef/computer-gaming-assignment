﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftPaddle : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.up * Input.GetAxis("Vertical") * 35f * Time.deltaTime);


        //Screen Edge
        if (transform.position.y > 4f)
        {
            transform.position = new Vector3(transform.position.x,  4f);
        }

        if (transform.position.y < -4f)
        {

            transform.position = new Vector3(transform.position.x, -4f);
        }
    }
}
